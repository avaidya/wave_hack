#!/usr/bin/env python

from __future__ import division
import os
import sys
import itertools
from ROOT import *
#import numpy as np
import argparse

gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')

xAOD.Init().ignore()

 # 3) direct access to aux variable (when no accessor available)
# weight = getAux(tree.EventInfo___Nominal, "MCEventWeight", "float")

def getAux(auxObject, auxName, auxType = 'float'):
    return auxObject.auxdataConst(auxType)(auxName)


def getCutflow(indir, treename):


    EventCounter = 0

    c1 = TCanvas("c1", "Here's a title", 800 ,800)
    
    TH1.SetDefaultSumw2()


    outfile = TFile ("output.root", "RECREATE")

    deltaRmin_hist_low = TH1D("deltaRmin_hist1", "some title", 20, 0, 0.6)
    deltaRmin_hist_high = TH1D("deltaRmin_hist2", "some title", 20, 0, 0.6)


    jetN_hist = TH1D("JetN_hist", "some title", 20, 0, 100)
    rapidity_hist = TH1D ("rapidity_hist", "rapidity_hist", 60, -4., 4.)
    pt_hist = TH1D ("pt_hist", "pt_hist", 60, 0, 500 )
    
    rapidity_truth_hist = TH1D ("rapidity_truth_hist", "rapidity_hist", 60, -4., 4.)
    pt_truth_hist = TH1D ("pt_truth_hist", "pt_hist", 60, 0., 500)

    for i in range (10):

        globals()['rapidity_hist_'+ str(i)] = TH1D ("rapidity_hist"+ str(i), "rapidity_hist", 60, -4., 4.)
        globals()['pt_hist_'+ str(i)] = TH1D ("pt_hist"+ str(i), "pt_hist", 60, 0, 500.)
        globals()['deltaRmin_hist_low_'+ str(i)] = TH1D ('deltaRmin_hist_low_'+ str(i), "'deltaRmin_hist_low", 20, 0, 0.6)
        globals()['deltaRmin_hist_high_'+ str(i)] = TH1D ('deltaRmin_hist_high_'+ str(i), "'deltaRmin_hist_high", 20, 0, 0.6)


    infile = TFile.Open(indir  )
    tree = xAOD.MakeTransientTree(infile, treename)
        #tree= infile.Get(treename)

    entries = tree.GetEntries()
    print entries

        
        # loop ever event, see which selection cuts are passed
    for j in range (entries):
        EventCounter +=1

        tree.GetEntry(j)

        jetN_hist.Fill( tree.AntiKt4EMTowerJets.size() )
        for jet in tree.AntiKt4EMTowerJets:
            rapidity_hist.Fill( jet.rapidity () )
            pt_hist.Fill( jet.pt() / 1000 )

        for jet in tree.AntiKt4TruthJets:
            rapidity_truth_hist.Fill( jet.rapidity () )
            pt_truth_hist.Fill( jet.pt() / 1000 )
     


        vectorOfVectors = list()

        vectorOfVectors.append( tree.nw_jets_0 )
        vectorOfVectors.append( tree.nw_jets_1 )
        vectorOfVectors.append( tree.nw_jets_2 )
        vectorOfVectors.append( tree.nw_jets_3 )
        vectorOfVectors.append( tree.nw_jets_4 )
        vectorOfVectors.append( tree.nw_jets_5 )
        vectorOfVectors.append( tree.nw_jets_6 )
        vectorOfVectors.append( tree.nw_jets_7 )
        vectorOfVectors.append( tree.nw_jets_8 )
        vectorOfVectors.append( tree.nw_jets_9 )

        for i in range (10):

            for jet in vectorOfVectors[i]:

                globals()['rapidity_hist_'+ str(i)].Fill (jet.rapidity () )
                globals()['pt_hist_'+ str(i)].Fill ( jet.pt() / 1000 )


        for truth_jet in tree.AntiKt4TruthJets:

            if truth_jet.pt() /1000 < 10: 
                continue 
            truthp4 = TLorentzVector ()
            truthp4.SetPtEtaPhiM (truth_jet.pt() , truth_jet.eta() , truth_jet.phi(), truth_jet.m() )

            DeltaRminlist = list()
            for i in range (10):
                
                DeltaRmin = 0.6

                for jet in vectorOfVectors[i]:

                    jetp4 = TLorentzVector ()
                    truthp4.SetPtEtaPhiM (jet.pt() , jet.eta() , jet.phi(), jet.m() )
                    
                    DeltaR = jetp4.DeltaR ( truthp4 )

                    if DeltaR < DeltaRmin:
                        DeltaRmin = DeltaR

                if truth_jet.pt()/1000 < 50 :
                    globals()['deltaRmin_hist_low_'+ str(i)].Fill ( DeltaRmin )
                else:
                    globals()['deltaRmin_hist_high_'+ str(i)].Fill ( DeltaRmin )


    outfile.cd()

    rapidity_hist.Write()
    rapidity_hist.Draw()
    rapidity_hist.SetMinimum(0.)
    rapidity_truth_hist.Write()
    rapidity_truth_hist.Draw("SAME")
    rapidity_truth_hist.SetMarkerColor(kRed)

    pt_hist.Write()
    pt_truth_hist.Write()

    for i in range (10):
        globals()['rapidity_hist_'+ str(i)].Write()
        globals()['rapidity_hist_'+ str(i)].Draw("SAME")
        globals()['pt_hist_'+ str(i)].Write()
        globals()['deltaRmin_hist_low_'+ str(i)].Write()
        globals()['deltaRmin_hist_high_'+ str(i)].Write()

    c1.Print("nominal_rapidity.pdf")
    jetN_hist.Draw()
    c1.Print("nominal_Njets.pdf")

    pt_hist.Draw()
    c1.SetLogy()
    pt_truth_hist.Draw("SAME")

    for i in range (10):
        globals()['pt_hist_'+ str(i)].Draw("SAME")
        
    c1.Print("all_pt_jets.pdf")


indir = '/afs/cern.ch/work/a/avaidya/public/wavelets/biggerout.root'


treename = "CollectionTree"

getCutflow(indir, treename)
#print output[0], output[1], output[2], output[3], output[4], output[5]



if __name__ == "__main__":
    main()
