#!/usr/bin/env python
from __future__ import division
import os
import sys
import itertools
import argparse
import ROOT
#from ROOT import *

def main():

    parser = argparse.ArgumentParser(description='Towers dist')
    parser.add_argument('--inputdir',  type=str, default="default", help = 'Path to directory where the input is stored')
    parser.add_argument('--outputfile', type=str, default="outputfile", help = 'Path to output directory')
    parser.add_argument('--debug', type = int, default = 0, help = 'Printing messages on if set to 1')

    args = parser.parse_args()

    inputdir = args.inputdir
    outputfile = args.outputfile
    debug = args.debug

    InputFiles = os.listdir(inputdir)
    print InputFiles


    ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )
    ROOT.xAOD.Init() # Initialize the xAOD infrastructure
    #ROOT.Init.Ignore()

    ROOT.TH1.SetDefaultSumw2()

    output_f = ROOT.TFile(outputfile,"RECREATE")
    output_f.cd()     
    hist_EMTowerClusterFine_NW_ratio = ROOT.TH1D("hist_EMTowerClusterFine_NW_ratio", "EMTowerClusterFine NW r", 150, -1.5, 1.5)
    hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut = ROOT.TH1D("hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut", "EMTowerClusterFine NW r, E > 0 , |y| < 3.2", 150, -1.5, 1.5)
    hist_EMTowerClusterFine_pt = ROOT.TH1D("hist_EMTowerClusterFine_pt", "EMTowerClusterFine pt", 40, 0, 4) # in GeV
    hist_EMTowerClusterFine_rapidity = ROOT.TH1D("hist_EMTowerClusterFine_rapidity", "EMTowerClusterFine y", 320, -4.0, 4.0)
    hist_EMTowerClusterFine_rapidity_phi = ROOT.TH2D("hist_EMTowerClusterFine_rapidity_phi", "EMTowerClusterFine y vs phi", 320, -4.0, 4.0, 320, -4.0, 4.0)
    hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut = ROOT.TH2D("hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut", "EMTowerClusterFine y vs phi, NW r > 0.7", 320, -4.0, 4.0, 320, -4.0, 4.0)

    hist_EMTowerClusterFine_sum_energy = ROOT.TH1D("hist_EMTowerClusterFine_sum_energy", "EMTowerClusterFine Energy Sum", 140, 0, 14000) # in GeV
    hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut = ROOT.TH1D("hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut", "EMTowerClusterFine Energy Sum, NW r > 0.3", 140, 0, 14000) # in GeV
    hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut = ROOT.TH1D("hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut", "EMTowerClusterFine Energy Sum, NW r > 0.4", 140, 0, 14000) # in GeV
    hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut = ROOT.TH1D("hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut", "EMTowerClusterFine Energy Sum, NW r > 0.5", 140, 0, 14000) # in GeV
    hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut = ROOT.TH1D("hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut", "EMTowerClusterFine Energy Sum, NW r > 0.6", 140, 0, 14000) # in GeV
    hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut = ROOT.TH1D("hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut", "EMTowerClusterFine Energy Sum, NW r > 0.7", 140, 0, 14000) # in GeV
    hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut = ROOT.TH1D("hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut", "EMTowerClusterFine Energy Sum, NW r > 0.8", 140, 0, 14000) # in GeV



    treeName = "CollectionTree"
    mc_weight = 0
    sum_tower_energy = 0 
    NW_ratio = -100000.0
    i = 0 

    sum_tower_energy_NW_ratio_03cut = 0 
    sum_tower_energy_NW_ratio_04cut = 0 
    sum_tower_energy_NW_ratio_05cut = 0 
    sum_tower_energy_NW_ratio_06cut = 0 
    sum_tower_energy_NW_ratio_07cut = 0 
    sum_tower_energy_NW_ratio_08cut = 0 

    for f in InputFiles:
        file = ROOT.TFile.Open(inputdir + "/" + f)
#        if ( "biggerout.root" not in f  ): 
        if ( "smallOut.root" not in f  ): 
            continue
        t = ROOT.xAOD.MakeTransientTree( file, treeName) # Make the "transient tree"  

        # Print some information:
        print( "Number of input events: %s" % t.GetEntries() )
        for entry in xrange( t.GetEntries() ):
#        for entry in xrange( 50 ):
            t.GetEntry( entry )
            #mc_weight = t.EventInfo.mcEventWeight()
            mc_weight = 1
            if debug == 1:
                print mc_weight
            if (entry%20 == 0):
                print (entry)
            # clean sum_tower_energy for the event
            sum_tower_energy = 0 
            sum_tower_energy_NW_ratio_03cut = 0 
            sum_tower_energy_NW_ratio_04cut = 0 
            sum_tower_energy_NW_ratio_05cut = 0 
            sum_tower_energy_NW_ratio_06cut = 0 
            sum_tower_energy_NW_ratio_07cut = 0 
            sum_tower_energy_NW_ratio_08cut = 0 

            i = 0
            for EMTower_itr in t.EMTowerClusterFine:  # loop over electron collection
                i = i + 1
                hist_EMTowerClusterFine_pt.Fill(EMTower_itr.pt() / 1000.0,mc_weight)
                NW_ratio = getAux(EMTower_itr,"NW_ratio", "double")
                hist_EMTowerClusterFine_NW_ratio.Fill(NW_ratio, mc_weight)
                hist_EMTowerClusterFine_rapidity.Fill(EMTower_itr.rapidity(), mc_weight)
                hist_EMTowerClusterFine_rapidity_phi.Fill(EMTower_itr.rapidity(),EMTower_itr.phi(), mc_weight)
                if (NW_ratio >= 0.7):
                    hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut.Fill(EMTower_itr.rapidity(),EMTower_itr.phi(), mc_weight)
                if debug == 1:
                    print( "  EMTowerClusterFine pt = %g" %  ( EMTower_itr.pt() / 1000.0 ) )
                    print("EMTowerClusterFine NW_ratio = %g" %  ( NW_ratio ))
                # apply energy and eta cut
                if EMTower_itr.e() / 1000.0 <= 0 or EMTower_itr.rapidity() > 3.2 or EMTower_itr.rapidity() < -3.2:
                    continue
                hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut.Fill(NW_ratio, mc_weight)
                sum_tower_energy = sum_tower_energy +  EMTower_itr.e() / 1000.0
                if (NW_ratio >= 0.3):
                     sum_tower_energy_NW_ratio_03cut = sum_tower_energy_NW_ratio_03cut +  EMTower_itr.e() / 1000.0
                if (NW_ratio >= 0.4):
                     sum_tower_energy_NW_ratio_04cut = sum_tower_energy_NW_ratio_04cut +  EMTower_itr.e() / 1000.0
                if (NW_ratio >= 0.5):
                     sum_tower_energy_NW_ratio_05cut = sum_tower_energy_NW_ratio_05cut +  EMTower_itr.e() / 1000.0
                if (NW_ratio >= 0.6):
                     sum_tower_energy_NW_ratio_06cut = sum_tower_energy_NW_ratio_06cut +  EMTower_itr.e() / 1000.0
                if (NW_ratio >= 0.7):
                     sum_tower_energy_NW_ratio_07cut = sum_tower_energy_NW_ratio_07cut +  EMTower_itr.e() / 1000.0
                if (NW_ratio >= 0.8):
                     sum_tower_energy_NW_ratio_08cut = sum_tower_energy_NW_ratio_08cut +  EMTower_itr.e() / 1000.0
                if debug == 1:
                    print("EMTowerClusterFine e = %g" % (EMTower_itr.e() / 1000.0 ))
                    print("EMTowerClusterFine sum_tower_energy = %g" %  ( sum_tower_energy ))

            hist_EMTowerClusterFine_sum_energy.Fill(sum_tower_energy, mc_weight)

            hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut.Fill(sum_tower_energy_NW_ratio_03cut, mc_weight)
            hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut.Fill(sum_tower_energy_NW_ratio_04cut, mc_weight)
            hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut.Fill(sum_tower_energy_NW_ratio_05cut, mc_weight)
            hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut.Fill(sum_tower_energy_NW_ratio_06cut, mc_weight)
            hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut.Fill(sum_tower_energy_NW_ratio_07cut, mc_weight)
            hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut.Fill(sum_tower_energy_NW_ratio_08cut, mc_weight)
            if debug == 1:
                print ("event sum_tower_energy = %g" % (sum_tower_energy) )
                print ("n - towers: %g " % (i))
        file.Close()
        # end of the files list
    output_f.cd()     


    hist_EMTowerClusterFine_pt.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_pt.GetXaxis().SetTitle("p_{T} [GeV]")
    hist_EMTowerClusterFine_pt.Write()

    hist_EMTowerClusterFine_rapidity.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_rapidity.GetXaxis().SetTitle("y")
    hist_EMTowerClusterFine_rapidity.Write()

    hist_EMTowerClusterFine_rapidity_phi.GetXaxis().SetTitle("y")
    hist_EMTowerClusterFine_rapidity_phi.GetYaxis().SetTitle("#phi")
    hist_EMTowerClusterFine_rapidity_phi.Write()

    hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut.GetXaxis().SetTitle("y")
    hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut.GetYaxis().SetTitle("#phi")
    hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut.Write()

    hist_EMTowerClusterFine_NW_ratio.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_NW_ratio.GetXaxis().SetTitle("r")
    hist_EMTowerClusterFine_NW_ratio.Write()

    hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut.GetXaxis().SetTitle("r")
    hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut.Write()


    hist_EMTowerClusterFine_sum_energy.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_sum_energy.GetXaxis().SetTitle("Sum E [GeV]")
    hist_EMTowerClusterFine_sum_energy.Write()


    hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut.GetXaxis().SetTitle("Sum E [GeV]")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut.Write()

    hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut.GetXaxis().SetTitle("Sum E [GeV]")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut.Write()


    hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut.GetXaxis().SetTitle("Sum E [GeV]")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut.Write()

    hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut.GetXaxis().SetTitle("Sum E [GeV]")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut.Write()

    hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut.GetXaxis().SetTitle("Sum E [GeV]")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut.Write()

    hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut.GetYaxis().SetTitle("N")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut.GetXaxis().SetTitle("Sum E [GeV]")
    hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut.Write()

    output_f.Close()

def getAux(auxObject, auxName, auxType = 'float'):
    return auxObject.auxdataConst(auxType)(auxName)


if __name__ == "__main__":
    main()


