void figures(){


  TFile * hist_file = TFile::Open("data-JSSxAOD.root");


  TH2 * hist_EMTowerClusterFine_rapidity_phi                    = (TH2*) hist_file->Get("hist_EMTowerClusterFine_rapidity_phi");
  TH2 * hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut    =  (TH2*) hist_file->Get("hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut");

  TH1 *  hist_EMTowerClusterFine_pt                                =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_pt");
  TH1 *  hist_EMTowerClusterFine_rapidity                          =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_rapidity");
  TH1 *  hist_EMTowerClusterFine_NW_ratio                          =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_NW_ratio");
  TH1 *  hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut    =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut");
  TH1 *  hist_EMTowerClusterFine_sum_energy                        =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_sum_energy");
  TH1 *  hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut        =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut");
  TH1 *  hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut        =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut");
  TH1 *  hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut        =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut");
  TH1 *  hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut        =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut");
  TH1 *  hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut        =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut");
  TH1 *  hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut        =  (TH1*) hist_file->Get("hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut");

  gStyle->SetOptStat(0);

  TCanvas *c_1 = new TCanvas();
  c_1->Clear();
  hist_EMTowerClusterFine_rapidity_phi->Draw("colz");
  c_1->SaveAs("FiguresTowers/hist_EMTowerClusterFine_rapidity_phi.pdf");
  c_1->SetLogz();
  c_1->SaveAs("FiguresTowers/hist_EMTowerClusterFine_rapidity_phi_log.pdf");

  TCanvas *c_2 = new TCanvas();
  c_2->Clear();
  hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut->Draw("colz");
  c_2->SaveAs("FiguresTowers/hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut.pdf");
  c_2->SetLogz();
  c_2->SaveAs("FiguresTowers/hist_EMTowerClusterFine_rapidity_phi__NW_ratio_07cut_log.pdf");

  TCanvas *c_3 = new TCanvas();
  c_3->Clear();
  hist_EMTowerClusterFine_pt->Draw();
  c_3->SaveAs("FiguresTowers/hist_EMTowerClusterFine_pt.pdf");
  c_3->SetLogy();
  c_3->SaveAs("FiguresTowers/hist_EMTowerClusterFine_pt_log.pdf");


  TCanvas *c_4 = new TCanvas();
  c_4->Clear();
  hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut->Draw();
  c_4->SaveAs("FiguresTowers/hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut.pdf");
  c_4->SetLogy();
  c_4->SaveAs("FiguresTowers/hist_EMTowerClusterFine_NW_ratio_after_pt_rapidity_cut_log.pdf");


  TCanvas *c_4_2 = new TCanvas();
  c_4_2->Clear();
  hist_EMTowerClusterFine_NW_ratio->Draw();
  c_4_2->SaveAs("FiguresTowers/hist_EMTowerClusterFine_NW_ratio.pdf");
  c_4_2->SetLogy();
  c_4_2->SaveAs("FiguresTowers/hist_EMTowerClusterFine_NW_ratio_log.pdf");


  hist_EMTowerClusterFine_sum_energy                 -> SetLineColor(1);
  hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut -> SetLineColor(2);
  hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut -> SetLineColor(3);
  hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut -> SetLineColor(4);
  hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut -> SetLineColor(5);
  hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut -> SetLineColor(6);
  hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut -> SetLineColor(7);

  gStyle->SetOptTitle(0);
  TCanvas *c_5 = new TCanvas();
  c_5->Clear();

  TLegend *leg = new TLegend(0.72,0.6,0.88,0.9);
  leg->AddEntry(hist_EMTowerClusterFine_sum_energy                ,"no r cut"    ,"l");
  leg->AddEntry(hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut,"r > 0.3 cut","l");
  leg->AddEntry(hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut,"r > 0.4 cut" ,"l");
  leg->AddEntry(hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut,"r > 0.5 cut","l");
  leg->AddEntry(hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut,"r > 0.6 cut","l");
  leg->AddEntry(hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut,"r > 0.7 cut","l");
  leg->AddEntry(hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut,"r > 0.8 cut","l");

  hist_EMTowerClusterFine_sum_energy->Draw();
  hist_EMTowerClusterFine_sum_energy__NW_ratio_03cut ->Draw("same");
  hist_EMTowerClusterFine_sum_energy__NW_ratio_04cut ->Draw("same");
  hist_EMTowerClusterFine_sum_energy__NW_ratio_05cut ->Draw("same");
  hist_EMTowerClusterFine_sum_energy__NW_ratio_06cut ->Draw("same");
  hist_EMTowerClusterFine_sum_energy__NW_ratio_07cut ->Draw("same");
  hist_EMTowerClusterFine_sum_energy__NW_ratio_08cut ->Draw("same");
  leg->Draw();
  c_5->SaveAs("FiguresTowers/hist_EMTowerClusterFine_sum_energy_overlaid.pdf");
  c_5->SetLogy();
  c_5->SaveAs("FiguresTowers/hist_EMTowerClusterFine_sum_energy_overlaid_log.pdf");

}

