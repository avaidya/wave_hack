#!/usr/bin/env python
from __future__ import division
import os
import sys
import itertools
import argparse

def getAux(auxObject, auxName, auxType = 'float'):
    return auxObject.auxdataConst(auxType)(auxName)


parser = argparse.ArgumentParser(description='Towers dist')
parser.add_argument('--inputdir',  type=str, default="default", help = 'Path to directory where the input is stored')
parser.add_argument('--outputfile', type=str, default="outputfile", help = 'Path to output directory')
parser.add_argument('--nEvents', '-n', type=int, default=-1, help = 'Number of events to process per file')
parser.add_argument('--debug', type = int, default = 0, help = 'Printing messages on if set to 1')

args = parser.parse_args()

inputdir = args.inputdir
outputfile = args.outputfile
nEvents = args.nEvents
debug = args.debug

InputFiles = os.listdir(inputdir)
print InputFiles

import ROOT
from ROOT import TTree

nBELayers = 4

ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )
ROOT.xAOD.Init() # Initialize the xAOD infrastructure
    #ROOT.Init.Ignore()

ROOT.TH1.SetDefaultSumw2()

output_f = ROOT.TFile(outputfile,"RECREATE")
output_f.cd()     

outputTree = TTree("CollectionTree", "JetHack ntuple")

treeName = "CollectionTree"
NW_ratio = -100000.0

twr_pt = ROOT.vector('double')()
twr_eta = ROOT.vector('double')()
twr_phi = ROOT.vector('double')()
twr_rValues = ROOT.vector('double')()
twr_eng_frac_em = ROOT.vector('double')()

twr_be = []
for i in range(nBELayers):
    twr_be.append(ROOT.vector('double')())

tru_jet_pt = ROOT.vector('double')()
tru_jet_eta = ROOT.vector('double')()
tru_jet_phi = ROOT.vector('double')()
tru_jet_m = ROOT.vector('double')()

truth_px = ROOT.vector('double')()
truth_py = ROOT.vector('double')()
truth_pz = ROOT.vector('double')()
truth_m = ROOT.vector('double')()
truth_id = ROOT.vector('int')()
truth_status = ROOT.vector('int')()

nw_jet_pt = []
nw_jet_eta = []
nw_jet_phi = []
nw_jet_m = []

for i in range(10):
  nw_jet_pt.append(ROOT.vector('double')())
  outputTree.Branch('nw_jets' + str(i) + '_pt', nw_jet_pt[i])

  nw_jet_eta.append(ROOT.vector('double')())
  outputTree.Branch('nw_jets' + str(i) + '_eta', nw_jet_eta[i])

  nw_jet_phi.append(ROOT.vector('double')())
  outputTree.Branch('nw_jets' + str(i) + '_phi', nw_jet_phi[i])

  nw_jet_m.append(ROOT.vector('double')())
  outputTree.Branch('nw_jets' + str(i) + '_m', nw_jet_m[i])


outputTree.Branch('twr_pt', twr_pt)
outputTree.Branch('twr_eta', twr_eta)    
outputTree.Branch('twr_phi', twr_phi)
outputTree.Branch('twr_NW_ratio', twr_rValues)
outputTree.Branch('twr_ENG_FRAC_EM', twr_eng_frac_em)

for i in range(nBELayers):
    outputTree.Branch('twr_energyBE_' + str(i), twr_be[i])

outputTree.Branch('AntiKt4TruthJets_pt', tru_jet_pt)
outputTree.Branch('AntiKt4TruthJets_eta', tru_jet_eta)
outputTree.Branch('AntiKt4TruthJets_phi', tru_jet_phi)
outputTree.Branch('AntiKt4TruthJets_m', tru_jet_m)

outputTree.Branch('TruthParticles_px', truth_px)
outputTree.Branch('TruthParticles_py', truth_py)
outputTree.Branch('TruthParticles_pz', truth_pz)
outputTree.Branch('TruthParticles_m', truth_m)
outputTree.Branch('TruthParticles_pdgId', truth_id)
outputTree.Branch('TruthParticles_status', truth_status)

for f in InputFiles:
    file = ROOT.TFile.Open(inputdir + "/" + f)
    t = ROOT.xAOD.MakeTransientTree( file, treeName) # Make the "transient tree"
    
    tEvents = t.GetEntries()
    maxEvents = nEvents
    if maxEvents > tEvents or maxEvents < 0:
        maxEvents=tEvents

    for entry in xrange( maxEvents ):
        t.GetEntry( entry )

        if (entry%20 == 0):
            print (entry)

        for twr in t.EMTowerClusterFine:  # loop over electron collection

            NW_ratio = getAux(twr,"NW_ratio", "double")

            if twr.e() / 1000.0 <= 0 or twr.rapidity() > 3.2 or twr.rapidity() < -3.2:
                continue

            twr_pt.push_back(twr.pt())
            twr_eta.push_back(twr.rapidity())
            twr_phi.push_back(twr.phi())
            twr_rValues.push_back(NW_ratio)
            twr_eng_frac_em.push_back(twr.getMomentValue(701))
            for i in range(nBELayers):
                twr_be[i].push_back(twr.energyBE(i))

        for jet in t.AntiKt4TruthJets:
            tru_jet_pt.push_back(jet.pt())
            tru_jet_eta.push_back(jet.eta())
            tru_jet_phi.push_back(jet.phi())
            tru_jet_m.push_back(jet.m())

        esum = 0.
        for p in t.TruthParticles:
            if p.status() != 1:
              continue
            esum = esum + p.e()
            truth_px.push_back(p.px())
            truth_py.push_back(p.py())
            truth_pz.push_back(p.pz())
            truth_m.push_back(p.m())
            truth_id.push_back(p.pdgId())
            truth_status.push_back(p.status())

        print "esum = " + str(esum)

        for i in range(10):
          for jet in getattr(t, "nw_jets_" + str(i)):
            nw_jet_pt[i].push_back(jet.pt())
            nw_jet_eta[i].push_back(jet.eta())
            nw_jet_phi[i].push_back(jet.phi())
            nw_jet_m[i].push_back(jet.m())

        outputTree.Fill()

        twr_pt.clear()
        twr_eta.clear()
        twr_phi.clear()
        twr_rValues.clear()
        twr_eng_frac_em.clear()
        for i in range(nBELayers):
            twr_be[i].clear()
                
        tru_jet_pt.clear()
        tru_jet_eta.clear()
        tru_jet_phi.clear()
        tru_jet_m.clear()

        truth_px.clear()
        truth_py.clear()
        truth_pz.clear()
        truth_m.clear()
        truth_id.clear()
        truth_status.clear()

        for i in range(10):
          nw_jet_pt[i].clear()
          nw_jet_eta[i].clear()
          nw_jet_phi[i].clear()
          nw_jet_m[i].clear()

    file.Close()
        # end of the files list
output_f.cd()     

output_f.Write()
output_f.Close()




