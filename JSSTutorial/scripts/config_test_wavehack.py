from xAH_config import xAH_config
from ROOT import vector 
c = xAH_config()

#c.setalg("BasicEventSelection", {"m_debug": False,
#                                 "m_isMC" : True,
#                                 "m_truthLevelOnly": False,
#                                 "m_applyGRLCut": False,
#                                 "m_doPUreweighting": False,
#                                 "m_vertexContainerName": "PrimaryVertices",
#                                 "m_PVNTrack": 2,
#                                 "m_applyPrimaryVertexCut": True,
#                                 "m_useMetaData": False,
#                                 "m_derivationName": "JETM8"
#                                })

c.setalg("JSSTutorialAlgo", {"m_debug": False,
    "m_name": "JSSTutorialAlgo",
    "m_MyNewVariable" : "ThisGotLoadedIn",
    "m_TreeName" : "JetTree"
})


# *******************************************************
# Write out a Mini xAOD
# -------------------------------

# prepare a vector of string.
import ROOT
selectedVars = ROOT.vector('string')()

containers = ["EMTowerClusterFine", "EventInfo", "AntiKt4TruthJets" ] # containers from input file
containers2 = [ "AntiKt4EMTowerJets" ] # build during jobs

for i in range(0, 10):
     containers2.append("nw_jets_" + str(i))

# we want to save out only a selected list of variables
# for each container build the string of what variable we want to save :
for jc in containers2:
     selectedVars.push_back(jc+"Aux.pt.eta.phi.m")


c.setalg("JSSMinixAOD", dict(m_debug= False,
                              m_outputFileName="JSSxAOD.root",
                              m_createOutputFile=True,
 
m_copyFileMetaData=True,m_copyCutBookkeeper=False,
                              m_simpleCopyKeys = ' '.join(containers),
                              m_storeCopyKeys=  ' '.join(containers2),
                              m_selectedAuxVars = selectedVars
                              ) )


# c.setalg("TopWBosonSubstructureAlgo", {"m_debug": False,
#     "m_name": "TopWBosonSubstructureAlgo",
#     "m_MyNewVariable" : "ThisGotLoadedIn",
#     "m_TreeName" : "JetTree"
# })

# You can add new parameters to your algorithm that will be loaded automatically.  
# Note that they will be loaded into the algorithms member variable that has the same name, only if it is public.
# The example above it the "m_MyNewVariable"
