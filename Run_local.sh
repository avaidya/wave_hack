#remove the directory where you previously output your test to
rm -rf OutputDirectory

#run your new test
#xAH_run.py --files /afs/cern.ch/work/m/meehan/public/HCW2016Tutorial/InputFile/mc15_13TeV.301256.Pythia8EvtGen_A14NNPDF23LO_Wprime_WZqqqq_m800.merge.DAOD_JETM8.e3743_s2608_s2183_r7772_r7676_p2613/DAOD_JETM8* \

#xAH_run.py --files /afs/cern.ch/work/a/avaidya/public/JetHack/xAODTower_10.root \
xAH_run.py --files /eos/atlas/user/l/lochp/public/ABOT2/CaloTowers/data/output/mc15_14TeV_vbf2600_FCal/TowerCluster.xAOD.prod001.pool.root \
--nevents 50 \
--config  JSSTutorial/scripts/config_test_wavehack.py \
-v \
--submitDir OutputDirectory \
direct
